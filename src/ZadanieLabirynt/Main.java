package ZadanieLabirynt;

import java.io.*;

public class Main {
    public static void main(String[] args) {

        File plik = new File("C:\\Users\\Anna1\\IdeaProjects\\AiSDLab2\\src\\ZadanieLabirynt\\labirynt5.txt");

        BufferedReader wczytaj = null;
        try {
            wczytaj = new BufferedReader(new FileReader(plik));
            String linia;
            linia = wczytaj.readLine();
            int rozmiar;
            String[] tablicaStringow = linia.split(" ");
            rozmiar = Integer.valueOf(tablicaStringow[0]);
            Labirynt labirynt = new Labirynt();
            Labirynt labirynt2 = new Labirynt();
            labirynt.setN(rozmiar);
            labirynt2.setN(rozmiar);
            char[][] tablica = new char[rozmiar][rozmiar];
            char[][] tablica2 = new char[rozmiar][rozmiar];
            for (int i = 0; i < rozmiar; i++) {
                linia = wczytaj.readLine();
                tablica[i] = linia.toCharArray();
                tablica2[i] = linia.toCharArray();
            }


            labirynt.setTab(tablica);
            labirynt.setIlePorownan(0);
            labirynt.sciezka(0, rozmiar-1);
            labirynt.wypiszTablice();
            System.out.println();
            System.out.println("Ilość porównań: " + labirynt.getIlePorownan());

            System.out.println();

            labirynt2.setTab(tablica2);
            labirynt2.setIlePorownan(0);
            labirynt2.sciezka2(0, rozmiar-1);
            labirynt2.wypiszTablice();
            System.out.println("Ilość porównań: " + labirynt2.getIlePorownan());

            File sciezka1 = new File("C:\\Users\\Anna1\\IdeaProjects\\AiSDLab2\\src\\ZadanieLabirynt\\sciezka5.txt");
            labirynt.wypiszDoPliku(sciezka1, tablica2);

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }

}



