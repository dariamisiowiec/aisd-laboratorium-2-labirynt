package ZadanieLabirynt;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Labirynt {

    private int n;
    private char[][] tab = new char[n][n];
    private int ilePorownan = 0;


    public boolean sciezka (int x, int y) {
        if(wspolrzednePozaObszaremLabiryntu(x, y))
            return false;
        if (wspolrzednePrzeszkody(x, y))
            return false;
        if (sciezkaEksplorowana(x, y))
            return false;
        if (wspolrzdneWyjscia(x, y)) {
            tab[x][y] = '*';
            return true;
        }
        tab[x][y] = ',';
        if (sciezka(x+1, y)) {
            tab[x][y] = '*';
            ilePorownan++;
            return true;
        }
        if (sciezka(x-1, y)) {
            tab[x][y] = '*';
            ilePorownan+=2;
            return true;
        }
        if (sciezka(x, y+1)){
            tab[x][y] = '*';
            ilePorownan+=3;
            return true;
        }
        if (sciezka(x, y-1)) {
            tab[x][y] = '*';
            ilePorownan+=4;
            return true;
        }
        ilePorownan +=4;
        return false;
    }

    public boolean sciezka2 (int x, int y) {
        if(wspolrzednePozaObszaremLabiryntu(x, y))
            return false;
        if (wspolrzednePrzeszkody(x, y))
            return false;
        if (sciezkaEksplorowana(x, y))
            return false;
        if (wspolrzdneWyjscia(x, y)) {
            tab[x][y] = '*';
            return true;
        }
        tab[x][y] = ',';


        if (sciezka2(x+1, y)) {
            tab[x][y] = '*';
            ilePorownan++;
            return true;
        }
        if (sciezka2(x, y-1)) {
            tab[x][y] = '*';
            ilePorownan+=2;
            return true;
        }
        if (sciezka2(x, y+1)){
            tab[x][y] = '*';
            ilePorownan+=3;
            return true;
        }
        if (sciezka2(x-1, y)) {
            tab[x][y] = '*';
            ilePorownan+=4;
            return true;
        }
        ilePorownan +=4;
        return false;
    }


    private boolean wspolrzdneWyjscia(int x, int y) {
        return (x==n-1 && y==0);
    }

    private boolean wspolrzednePozaObszaremLabiryntu(int x, int y) {
        return (x>0 && x>n && y<0 && y>n);
    }

    private boolean wspolrzednePrzeszkody(int x, int y){
       try {
           return(tab[x][y]=='#'); }
           catch (ArrayIndexOutOfBoundsException a){
           return true;
        }
    }

    private boolean sciezkaEksplorowana(int x, int y) {
        return (tab[x][y] == ',');
    }


    public void setN(int n) {
        this.n = n;
    }

    public void setTab(char[][] tab) {
        this.tab = tab;
    }

    public int getIlePorownan() {
        return ilePorownan;
    }

    public void setIlePorownan(int ilePorownan) {
        this.ilePorownan = ilePorownan;
    }

    public void wypiszTablice() {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(tab[i][j]);
            }
            System.out.println();
        }
    }

    public void wypiszDoPliku(File file, char[][] array) {

        try (PrintWriter writer = new PrintWriter(new FileWriter(file, true))) {
            for (int i = 0; i < array.length; i++) {
                for (int j = 0; j < array.length; j++) {
                    writer.print(array[i][j]);
                }
                writer.println();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

}
