package LiczbyLucasa;

public class Main {
    public static void main(String[] args) {

        for (int i = 0; i < 10; i++) {
            System.out.print(liczbaLucasa(i) + " ");
        }

    }

    public static int liczbaLucasa(int n) {
        if (n == 0)
            return 2;
        else if (n==1)
            return 1;
        else
            return liczbaLucasa(n-1) + liczbaLucasa(n-2);

    }
}
